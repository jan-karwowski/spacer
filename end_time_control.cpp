#include "end_time_control.hxx"

#include <wx/timectrl.h>

namespace tim = boost::posix_time;

namespace spacer {

end_time_control::end_time_control(wxWindow *parent, wxWindowID windowID)
    : wxPanel(parent, windowID) {
  auto sizer = new wxBoxSizer(wxVERTICAL);
  auto time_picker = new wxTimePickerCtrl(this, wxID_ANY);
  auto set_time = new wxButton(this, wxID_ANY, _("Set end time"));
  remaining_display = new wxStaticText(this, wxID_ANY, "Not started");
  sizer->Add(time_picker);
  sizer->Add(set_time);
  sizer->Add(remaining_display);
  this->SetSizerAndFit(sizer);

  set_time->Bind(wxEVT_BUTTON, [this, time_picker](const wxCommandEvent &) {
    int hour;
    int minute;
    int sec;
    time_picker->GetTime(&hour, &minute, &sec);
    auto now = tim::second_clock::local_time();
    end_time = tim::ptime(now.date(), tim::hours(hour) + tim::minutes(minute) +
                                          tim::seconds(sec));
    timer_step();
  });
  timer.Bind(wxEVT_TIMER, [this](const wxTimerEvent &) { this->timer_step(); });
}

void end_time_control::timer_step() {
  assert(end_time.has_value());
  // The timer callback is only called when time is set!

  auto diff = *end_time - tim::second_clock::local_time();
  if (diff.is_negative()) {
    time_elapsed();
    remaining_display->SetLabel("Time elapsed!");
  } else if (diff <= tim::seconds(90)) {
    timer.StartOnce(1000);
    remaining_display->SetLabel(
        wxString::Format(wxT("%ld seconds remaining"), diff.total_seconds()));
  } else {
    timer.StartOnce(1000 * diff.total_seconds() % 60);
    remaining_display->SetLabel(wxString::Format(wxT("%ld minutes remaining"),
                                                 diff.total_seconds() / 60));
  }
}

} // namespace spacer
