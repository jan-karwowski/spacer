#ifndef TIMER_CONTROL_H
#define TIMER_CONTROL_H

#include <optional>

#include <boost/date_time.hpp>
#include <boost/signals2.hpp>

#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

namespace spacer {

//TODO external event/control
class end_time_control : public wxPanel {
public:
  end_time_control(wxWindow *parent, wxWindowID = wxID_ANY);
  boost::signals2::signal<void()> time_elapsed;
  
private:
  wxTimer timer;
  std::optional<boost::posix_time::ptime> end_time;
  wxStaticText *remaining_display;
  void timer_step();
};
} // namespace spacer

#endif /* TIMER_CONTROL_H */
