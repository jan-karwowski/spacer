#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "end_time_control.hxx"

#include <wx/notifmsg.h>

namespace spacer {

class SpacerApp : public wxApp {
public:
  virtual bool OnInit();
};

enum MainWindowEvId {};

class MainWindow : public wxFrame {
public:
  MainWindow();

private:
  void OnExit(wxCommandEvent &event);
  wxDECLARE_EVENT_TABLE();

  wxNotificationMessage notification;
};

bool SpacerApp::OnInit() {
  MainWindow *window = new MainWindow();
  window->Show(true);
  return true;
}

MainWindow::MainWindow() : wxFrame(NULL, wxID_ANY, "Spacer") {
  wxMenu *programMenu = new wxMenu;
  programMenu->Append(wxID_EXIT, "E&xit");
  wxMenuBar *menuBar = new wxMenuBar;
  menuBar->Append(programMenu, "&Program");

  SetMenuBar(menuBar);

  wxBoxSizer *mainLayout = new wxBoxSizer(wxHORIZONTAL);

  auto etc = new end_time_control(this);
  mainLayout->Add(etc);

  this->SetSizerAndFit(mainLayout);

  etc->time_elapsed.connect([this] () {
    notification.SetTitle("Date timer elapsed");
    notification.SetMessage("Date timer elapsed");
    notification.Show();
  });
}

void MainWindow::OnExit(wxCommandEvent &) { Close(true); }

wxBEGIN_EVENT_TABLE(MainWindow, wxFrame) EVT_MENU(wxID_EXIT, MainWindow::OnExit)
    wxEND_EVENT_TABLE()

} // namespace spacer

wxIMPLEMENT_APP(spacer::SpacerApp);
